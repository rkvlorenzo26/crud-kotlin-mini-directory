package com.example.escience.crudproject.utils

import com.example.escience.crudproject.entity.Person

class Validations {
//    fun validateForm(firstName : String, lastName: String, address: String, contactNo: String) : Boolean = firstName.isEmpty() || lastName.isEmpty() || address.isEmpty() || contactNo.isEmpty()
//    fun validateContactNo(contactNo : String) : Boolean = contactNo.length != 11

    fun validateForm(person : Person)  : Boolean = person.firstName!!.isEmpty() || person.lastName!!.isEmpty() || person.address!!.isEmpty() || person.contactNo!!.isEmpty()
    fun validateContactNo(person : Person) : Boolean = person.contactNo!!.length != 11
}