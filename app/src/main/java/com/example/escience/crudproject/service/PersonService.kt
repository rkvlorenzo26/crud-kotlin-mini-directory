package com.example.escience.crudproject.service

import android.content.Context
import com.example.escience.crudproject.dao.PersonDao
import com.example.escience.crudproject.database.DBHelper
import com.example.escience.crudproject.entity.Results
import com.example.escience.crudproject.entity.Person
import com.example.escience.crudproject.enums.CodeType
import com.example.escience.crudproject.enums.MessageType
import com.example.escience.crudproject.utils.Validations

class PersonService : PersonDao {

    private var dbHelper : DBHelper

    constructor(context : Context) {
        dbHelper = DBHelper(context, null)
    }

    override fun saveData(person: Person): Results {
        when (true) {
            Validations().validateForm(person) -> return Results(CodeType.FAILED.code, MessageType.INCOMPLETE_FORM.message)
            Validations().validateContactNo(person) -> return Results(CodeType.FAILED.code, MessageType.INVALID_CONTACT.message)
            else -> {
                if (dbHelper.saveData(person)) {
                    return Results(CodeType.SUCCESS.code, MessageType.SAVE_SUCCESS.message)
                }
                return Results(CodeType.FAILED.code, MessageType.SAVE_FAILED.message)
            }
        }
    }

    override fun updateData(person: Person): Results {
        when (true) {
            Validations().validateForm(person) -> return Results(CodeType.FAILED.code, MessageType.INCOMPLETE_FORM.message)
            Validations().validateContactNo(person) -> return Results(CodeType.FAILED.code, MessageType.INVALID_CONTACT.message)
            else -> {
                if (dbHelper.updateData(person)) {
                    return Results(CodeType.SUCCESS.code, MessageType.UPDATE_SUCCESS.message)
                }
                return Results(CodeType.FAILED.code, MessageType.UPDATE_FAILED.message)
            }
        }
    }

    override fun deleteData(id: Int?): Int {
        return dbHelper.deleteData(id)
    }

    override fun getAllData(): ArrayList<Person> {
        return dbHelper.getAllData()
    }
}