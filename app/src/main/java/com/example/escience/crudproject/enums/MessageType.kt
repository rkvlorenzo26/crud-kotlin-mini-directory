package com.example.escience.crudproject.enums

enum class MessageType (val message : String){

    //Add Edit
    INCOMPLETE_FORM("Please fill up all fields."),
    INVALID_CONTACT("Contact no. should be 11 digits."),

    //Delete
    DELETE_SUCCESS("Entry successfully deleted."),
    DELETE_FAILED("Unable to delete selected entry."),

    //ADD
    SAVE_SUCCESS("Entry successfully added."),
    SAVE_FAILED("Unable to add new entry."),

    UPDATE_SUCCESS("Entry successfully updated."),
    UPDATE_FAILED("Unable to update selected entry."),
}