package com.example.escience.crudproject.utils

import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.example.escience.crudproject.entity.Person

class ImageGenerator {
    fun generateImage(person : Person) : TextDrawable {
        //Get Initials
        val initials : String = "" + person.firstName?.get(0) + person.lastName?.get(0)

        //Generate Colored Initials
        val color = ColorGenerator.MATERIAL.getColor(initials)
        return TextDrawable.builder()
                .buildRound(initials, color)
    }
}