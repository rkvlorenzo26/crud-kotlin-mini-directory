package com.example.escience.crudproject.utils

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.escience.crudproject.R
import com.example.escience.crudproject.entity.Person
import com.example.escience.crudproject.entity.Results
import com.example.escience.crudproject.enums.CodeType
import com.example.escience.crudproject.enums.MessageType
import com.example.escience.crudproject.service.PersonService
import kotlinx.android.synthetic.main.layout_dialog_box.view.*

class CustomArrayAdapter(private val context: Context,
                         private val dataSource: ArrayList<Person>) : BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private var personService : PersonService? = PersonService(context)

    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun isEnabled(position: Int): Boolean {
        return false
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView = inflater.inflate(R.layout.layout_listview, parent, false)

        val title = rowView.findViewById<TextView>(R.id.title)
        val address = rowView.findViewById<TextView>(R.id.lblAddress)
        val contact = rowView.findViewById<TextView>(R.id.lblContact)
        val img = rowView.findViewById<ImageView>(R.id.imageView)
        val btnEdit = rowView.findViewById<ImageButton>(R.id.btnEdit)
        val btnDelete = rowView.findViewById<ImageButton>(R.id.btnDelete)

        val person = getItem(position) as Person
        title.text = person.firstName + " " + person.lastName
        address.text = person.address
        contact.text = person.contactNo

        img.setImageDrawable(ImageGenerator().generateImage(person))
        btnEdit.setOnClickListener({ _ : View? -> buildUpdateDialog(context, person)})
        btnDelete.setOnClickListener({ _ : View? -> buildDeleteDialog(context, person.id) })

        return rowView
    }

    private fun buildDeleteDialog(context : Context, id : Int?) {
        val mBuilder = AlertDialog.Builder(context)
                .setMessage("Delete this record?")

        mBuilder.setPositiveButton("Yes"){ _, _ ->
            if (personService?.deleteData(id) == 1) {
                Toast.makeText(context, MessageType.DELETE_SUCCESS.message, Toast.LENGTH_SHORT).show()
                updateDataSource()
            } else {
                Toast.makeText(context, MessageType.DELETE_FAILED.message, Toast.LENGTH_SHORT).show()
            }
        }
        mBuilder.setNegativeButton("No") { _, _ -> }
        mBuilder.show()
    }

    private fun buildUpdateDialog(context : Context, person : Person) {
        val mDialogView = LayoutInflater.from(context).inflate(R.layout.layout_dialog_box, null)
        val mBuilder = AlertDialog.Builder(context)
                .setView(mDialogView)
                .setTitle("Update")

        val mAlertDialog = mBuilder.show()

        mDialogView.firstName.setText(person.firstName)
        mDialogView.lastName.setText(person.lastName)
        mDialogView.address.setText(person.address)
        mDialogView.contactNo.setText(person.contactNo)

        mDialogView.btnSubmit.setOnClickListener{
            val firstName = mDialogView.firstName.text.toString().trim()
            val lastName = mDialogView.lastName.text.toString().trim()
            val address = mDialogView.address.text.toString().trim()
            val contactNo = mDialogView.contactNo.text.toString().trim()

            val result : Results = personService!!.updateData(Person(Integer.parseInt(person.id.toString()), firstName, lastName, address, contactNo))

            if (result.code == CodeType.SUCCESS.code) {
                updateDataSource()
                mAlertDialog.dismiss()
            } else {
                Toast.makeText(context, result.message, Toast.LENGTH_SHORT).show()
            }
        }

        mDialogView.btnCancel.setOnClickListener { mAlertDialog.dismiss() }
    }

    private fun updateDataSource() {
        dataSource.clear()
        dataSource.addAll(personService!!.getAllData())
        notifyDataSetChanged()
    }
}