package com.example.escience.crudproject.database

import android.content.Context
import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.example.escience.crudproject.entity.Person

class DBHelper(context: Context, factory: SQLiteDatabase.CursorFactory?) :
        SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {

    companion object {
        private const val DATABASE_VERSION = 2
        private const val DATABASE_NAME = "directory.db"
        const val TABLE_NAME = "tbl_info"
        const val COLUMN_ID = "id"
        const val COLUMN_FIRST_NAME = "first_name"
        const val COLUMN_LAST_NAME = "last_name"
        const val COLUMN_ADDRESS = "address"
        const val COLUMN_CONTACT_NO = "contact_no"
    }

    override fun onCreate(db: SQLiteDatabase) {
        val createTable = ("CREATE TABLE " +
                TABLE_NAME + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_FIRST_NAME + " TEXT,"
                + COLUMN_LAST_NAME + " TEXT,"
                + COLUMN_ADDRESS + " TEXT,"
                + COLUMN_CONTACT_NO + " TEXT" + ")")
        db.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }

    fun getAllData(): ArrayList<Person> {
        val query = "SELECT * FROM $TABLE_NAME ORDER BY $COLUMN_FIRST_NAME ASC"
        val db = this.readableDatabase
        val cursor = db.rawQuery(query, null)
        val personList = ArrayList<Person>()

        while (cursor.moveToNext()) {
            val person = Person(Integer.parseInt(cursor.getString(0)), cursor.getString(1),
            cursor.getString(2), cursor.getString(3), cursor.getString(4))
            personList.add(person)
        }
        cursor.close()
        return personList
    }

    fun saveData(person: Person) : Boolean{
        val values = ContentValues()
        values.put(COLUMN_FIRST_NAME, person.firstName)
        values.put(COLUMN_LAST_NAME, person.lastName)
        values.put(COLUMN_ADDRESS, person.address)
        values.put(COLUMN_CONTACT_NO, person.contactNo)

        val db = this.writableDatabase

        var result = false
        try {
            db.insert(TABLE_NAME, null, values)
            db.close()
            result = true
        } catch (e : Exception) {
            Log.e("DBHelper", e.message)
            db.close()
        }
        return result
    }

    fun updateData(person: Person) : Boolean {
        val values = ContentValues()
        values.put(COLUMN_FIRST_NAME, person.firstName)
        values.put(COLUMN_LAST_NAME, person.lastName)
        values.put(COLUMN_ADDRESS, person.address)
        values.put(COLUMN_CONTACT_NO, person.contactNo)

        val db = this.writableDatabase
        val id = person.id.toString()

        var result = false
        try {
            db.update(TABLE_NAME, values, String.format("%s = ?", COLUMN_ID), arrayOf(id))
            db.close()
            result = true
        } catch (e : Exception) {
            Log.e("DBHelper", e.message)
            db.close()
        }
        return result
    }

    fun deleteData(id: Int?) : Int {
        val query = "SELECT * FROM $TABLE_NAME WHERE $COLUMN_ID = \"$id\""
        val db = this.writableDatabase
        val cursor = db.rawQuery(query, null)

        var result = 0
        if (cursor.moveToFirst()) {
            val id = Integer.parseInt(cursor.getString(0))
            try {
                result = db.delete(TABLE_NAME, String.format("%s = ?", COLUMN_ID), arrayOf(id.toString()))
                db.close()
            } catch (e : Exception) {
                Log.e("DBHelper", e.message)
                db.close()
            }
        }
        return result
    }
}