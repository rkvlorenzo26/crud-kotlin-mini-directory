package com.example.escience.crudproject.entity

class Person {
    var id : Int? = 0
    var firstName : String? = null
    var lastName : String? = null
    var address : String? = null
    var contactNo : String? = null

    constructor(firstName : String, lastName: String, address: String, contactNo: String) {
        this.firstName = firstName
        this.lastName = lastName
        this.address = address
        this.contactNo = contactNo
    }

    constructor(id: Int, firstName: String, lastName: String, address: String, contactNo: String) {
        this.id = id
        this.firstName = firstName
        this.lastName = lastName
        this.address = address
        this.contactNo = contactNo
    }

    override fun toString(): String {
        return "Person(id=$id, firstName=$firstName, lastName=$lastName, address=$address, contactNo=$contactNo)"
    }

}