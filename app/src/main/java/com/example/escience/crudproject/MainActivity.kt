package com.example.escience.crudproject

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.widget.ListView
import android.widget.Toast
import com.example.escience.crudproject.entity.Results
import com.example.escience.crudproject.entity.Person
import com.example.escience.crudproject.enums.CodeType
import com.example.escience.crudproject.service.PersonService
import com.example.escience.crudproject.utils.CustomArrayAdapter
import kotlinx.android.synthetic.main.layout_dialog_box.view.*

class MainActivity : AppCompatActivity() {

    private var customAdapter: CustomArrayAdapter? = null
    private var personService : PersonService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        personService = PersonService(this)
        updateListView()
    }

    private fun updateListView() {
        customAdapter = CustomArrayAdapter(this, personService!!.getAllData())
        val listView = findViewById<ListView>(R.id.listView)
        listView.adapter = customAdapter
    }

    private fun addAction() {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.layout_dialog_box, null)
        val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)
                .setTitle("New")

        val mAlertDialog = mBuilder.show()

        mDialogView.btnSubmit.setOnClickListener {
            val firstName = mDialogView.firstName.text.toString().trim()
            val lastName = mDialogView.lastName.text.toString().trim()
            val address = mDialogView.address.text.toString().trim()
            val contactNo = mDialogView.contactNo.text.toString().trim()

            val result : Results = personService!!.saveData(Person(firstName, lastName, address, contactNo))

            if (result.code == CodeType.SUCCESS.code) {
                updateListView()
                mAlertDialog.dismiss()
            } else {
                Toast.makeText(this, result.message, Toast.LENGTH_SHORT).show()
            }
        }

        mDialogView.btnCancel.setOnClickListener {
            mAlertDialog.dismiss()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menuAdd -> {
                addAction()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}

