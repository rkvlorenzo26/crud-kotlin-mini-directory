package com.example.escience.crudproject.enums

enum class CodeType (val code : String) {
    SUCCESS("1"),
    FAILED("-1")
}