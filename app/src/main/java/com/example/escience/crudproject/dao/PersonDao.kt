package com.example.escience.crudproject.dao

import com.example.escience.crudproject.entity.Results
import com.example.escience.crudproject.entity.Person

interface PersonDao {

    fun saveData(person: Person) : Results

    fun updateData(person: Person) : Results

    fun deleteData(id: Int?) : Int

    fun getAllData(): ArrayList<Person>?

}