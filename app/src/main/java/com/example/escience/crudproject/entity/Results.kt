package com.example.escience.crudproject.entity

class Results {
    var code : String? = null
    var message : String? = null

    constructor(code : String, message: String) {
        this.code = code
        this.message = message
    }
}